#ifndef GLOBALS_H
#define GLOBALS_H

#include <QMainWindow>
#include <QImage>

#include <QTcore/QtGlobal>

// ALL THE GLOBAL DECLARATIONS

extern QImage currentImage;
extern QImage originalImage;

#endif // GLOBALS_H
